import 'package:flutter/material.dart';
import 'package:flutter_turtle_graphics/flutter_turtle_graphics.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Turtle Graphics',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TurtlePath turtlePath;
  Path path;
  @override
  void initState() {
    createTurtle();
    super.initState();
  }

  void createTurtle() {
    // turtlePath = TurtlePath().genCurveC(100, 11);
    // turtlePath = TurtlePath().genCurveDragon(100, 11, 1);
    // turtlePath = TurtlePath().genKoch(375, 8, 60);
    // turtlePath =
    //     TurtlePath().genCurveSierpinskiTriangle(Point.zero, 200, 180, 7);
    turtlePath = TurtlePath().genSpiral(72, 60, 50, 0, 72);
    path = turtlePath.toPath;
  }

  @override
  void didUpdateWidget(covariant HomePage oldWidget) {
    setState(() {
      createTurtle();
    });
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: CustomPaint(
          painter: TurtlePainter(path: path, turtlePath: turtlePath),
          size: Size(screenSize.width / 2, screenSize.width / 2),
        ),
      ),
    );
  }
}

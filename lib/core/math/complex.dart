import 'math.dart' as Math;

class Complex {
  double re, im;

  Complex.id(this.re, this.im);

  Complex(double re, double im) {
    this.re = re;
    this.im = im;
  }

  double abs() {
    return ComplexFunctions.abs(this);
  }

  double arg() {
    return ComplexFunctions.arg(this);
  }

  Complex add(double n) {
    return Complex.id(n + re, im);
  }

  Complex multiply(double n) {
    return Complex.id(n * re, n * im);
  }

  Complex pow(double n) {
    return ComplexFunctions.pow(this, n);
  }

  Complex addComplex(Complex z) {
    return ComplexFunctions.add(this, z);
  }

  Complex subtract(Complex z) {
    return ComplexFunctions.subtract(this, z);
  }

  Complex divide(Complex z) {
    return ComplexFunctions.divide(this, z);
  }

  bool equals(Complex z, double tolerance) {
    return ComplexFunctions.equals(this, z, tolerance);
  }

  double euclideanDistance(Complex z) {
    return ComplexFunctions.euclideanDistance(this, z);
  }

  @override
  String toString() {
    return '(${this.re},${this.im})';
  }
}

class ComplexFunctions {
  static Complex add(Complex z1, Complex z2) {
    return Complex.id(z1.re + z2.re, z1.im + z2.im);
  }

  static Complex subtract(Complex z1, Complex z2) {
    return Complex.id(z1.re - z2.re, z1.im - z2.im);
  }

  static Complex divide(Complex z1, Complex z2) {
    double k = Math.pow(z2.im, 2) + Math.pow(z2.re, 2);
    return Complex.id((z1.re * z2.re + z1.im * z2.im) / k,
        (z1.im * z2.re - z1.re * z2.im) / k);
  }

  static double abs(Complex z) {
    return Math.sqrt(Math.pow(z.re, 2) + Math.pow(z.im, 2));
  }

  static double arg(Complex z) {
    return Math.atan2(z.im, z.re);
  }

  static Complex pow(Complex z1, double z2) {
    double r = Math.pow(z1.abs(), z2);
    double theta = z2 * z1.arg();
    return Complex.id(Math.cos(theta) * r, Math.sin(theta) * r);
  }

  static bool equals(Complex z1, Complex z2, double tolerance) {
    return (euclideanDistance(z1, z2) <= tolerance);
  }

  static double euclideanDistance(Complex z1, Complex z2) {
    return Math.sqrt(Math.pow(z1.re - z2.re, 2) + Math.pow(z1.im - z2.im, 2));
  }
}

/*
import 'math.dart';

void Xacdinh_Tam_BanKinh_cuaduongtronqua3diemkhongthanghang(
    Point A, Point B, Point C, Point tam, double R) {
  Point trd_AB, trd_AC;
  Vector cp_AB, cp_AC;

  trd_AB.x = (A.x + B.x) / 2;
  trd_AB.y = (A.y + B.y) / 2;
  trd_AC.x = (A.x + C.x) / 2;
  trd_AC.y = (A.y + C.y) / 2;

  cp_AB.A = B.x - A.x;
  cp_AB.B = B.y - A.y;
  cp_AC.A = C.x - A.x;
  cp_AC.B = C.y - A.y;

  tam.x = ((cp_AB.A * trd_AB.x + cp_AB.B * trd_AB.y) * cp_AC.B -
          (cp_AC.A * trd_AC.x + cp_AC.B * trd_AC.y) * cp_AB.B) /
      (cp_AB.A * cp_AC.B - cp_AC.A * cp_AB.B);

  tam.y = ((cp_AC.A * trd_AC.x + cp_AC.B * trd_AC.y) * cp_AB.A -
          (cp_AB.A * trd_AB.x + cp_AB.B * trd_AB.y) * cp_AC.A) /
      (cp_AB.A * cp_AC.B - cp_AC.A * cp_AB.B);

  R = sqrt(pow(tam.x - A.x, 2) + pow(tam.y - A.y, 2));
}

void Xacdinh_Tam_BanKinh_duongtronnoitieptamgiac(
    Point A, Point B, Point C, Point tam, double R) {
  double A1, B1, C1, A2, B2, C2, A3, B3, C3;
  double da1, db1, dc1, da2, db2, dc2;
  double delta1, delta2, delta3;
  double D;
  double signC1, signC2, signC3;

  A1 = A.y - B.y;
  B1 = B.x - A.x;
  C1 = -A1 * A.x - B1 * A.y;
  A2 = A.y - C.y;
  B2 = C.x - A.x;
  C2 = -A2 * C.x - B2 * C.y;
  A3 = C.y - B.y;
  B3 = B.x - C.x;
  C3 = -A3 * B.x - B3 * B.y;

// Xet dau delta , de xet lay tam nam trong tam giac , ko phai o ngoai
  signC1 = A1 * C.x + B1 * C.y + C1 > 0 ? 1 : -1;
  signC2 = A2 * B.x + B2 * B.y + C2 > 0 ? 1 : -1;
  signC3 = A3 * A.x + B3 * A.y + C3 > 0 ? 1 : -1;

  delta1 = sqrt(A1 * A1 + B1 * B1);
  delta2 = sqrt(A2 * A2 + B2 * B2);
  delta3 = sqrt(A3 * A3 + B3 * B3);

  delta1 *= signC1;
  delta2 *= signC2;
  delta3 *= signC3;
  A1 /= delta1;
  B1 /= delta1;
  C1 /= delta1;
  A2 /= delta2;
  B2 /= delta2;
  C2 /= delta2;
  A3 /= delta3;
  B3 /= delta3;
  C3 /= delta3;

  da1 = A1 - A2;
  db1 = B1 - B2;
  dc1 = C1 - C2;

  da2 = A1 - A3;
  db2 = B1 - B3;
  dc2 = C1 - C3;

  D = da1 * db2 - da2 * db1;
//// giai he 2 pt
  tam.y = (-da1 * dc2 + dc1 * da2) / D;
  tam.x = (-dc1 * db2 + db1 * dc2) / D;
/////////////////

  R = A1 * tam.x + B1 * tam.y + C1;
}

Map<String, dynamic>
    Xacdinh_Tam_cuaduongtrontiepxuc2duongtronchotruoc_voibankinhchotruoc(
        Point C1, Point C2, double R1, double R2, double R3) {
  var C3 = <Point>[];
  double R13, R23;
  R13 = abs(R1 - R3);
  R23 = abs(R2 - R3);

  var res = Giaodiemcua2duongtron(C1, C2, R13, R23);
  var gd = res['no'];
  var p1 = res['gd1'];
  var p2 = res['gd2'];
  if (gd > 0) {
    C3.add(p1);
    C3.add(p2);
  }
  R13 = abs(R1 - R3);
  R23 = abs(R2 + R3);

  res = Giaodiemcua2duongtron(C1, C2, R13, R23);
  gd = res['no'];
  p1 = res['gd1'];
  p2 = res['gd2'];
  if (gd > 0) {
    C3.add(p1);
    C3.add(p2);
  }
  R13 = abs(R1 + R3);
  R23 = abs(R2 - R3);

  res = Giaodiemcua2duongtron(C1, C2, R13, R23);
  gd = res['no'];
  p1 = res['gd1'];
  p2 = res['gd2'];
  if (gd > 0) {
    C3.add(p1);
    C3.add(p2);
  }
  R13 = abs(R1 + R3);
  R23 = abs(R2 + R3);

  res = Giaodiemcua2duongtron(C1, C2, R13, R23);
  gd = res['no'];
  p1 = res['gd1'];
  p2 = res['gd2'];
  if (gd > 0) {
    C3.add(p1);
    C3.add(p2);
  }

  return {'no': C3.length, 'C3': C3};
}

double Khoangcachgiua2diem(Point A, Point B) {
  return sqrt(pow(B.y - A.y, 2) + pow(B.x - A.x, 2));
}

void Xacdinh_Phuongtrinh_tieptuyenchung_cua2duongtron(
    Point C1, Point C2, double R1, double R2, List<LineEquation> PTTT) {
  double kc2tam = Khoangcachgiua2diem(C1, C2),
      hieu2bk = abs(R1 - R2),
      tong2bk = R1 + R2;

  int sogd2dtr = -1;
  Point gd1, gd2;

  //FIXME Giaodiemcua2duongtron(C1, C2, R1, R2, gd1, gd2, sogd2dtr);

  double AA, AAA, BBB, CCC, delta3, ac, bc, cc, at, bt, ct1, ct2, phi1, phi2;

  ac = C1.y - C2.y;
  bc = C2.x - C1.x;
  cc = -C1.y * C2.x + C1.x * C2.y;

  /// VE TIEP TUYEN NGOAI ////

  if (R1 == R2) // cung bk
  {
    if (kc2tam == 0) // vo so dg
      return;
    else {
      if (ac == 0) // C1C2 song song truc hoanh
      {
        PTTT[0].A = 0;
        PTTT[0].B = 1;
        PTTT[0].C = (C1.y - R1);
        PTTT[1].A = 0;
        PTTT[1].B = 1;
        PTTT[1].C = (C1.y + R1);
      } else if (bc == 0) // C1C2 song song truc tung
      {
        PTTT[0].A = 1;
        PTTT[0].B = 0;
        PTTT[0].C = (C1.x - R1);
        PTTT[1].A = 1;
        PTTT[1].B = 0;
        PTTT[1].C = (C1.x + R1);
      } else if (ac != 0 && bc != 0) {
        at = ac;
        bt = bc;
        AA = at * at + bt * bt;
        phi1 = 2 * at * C1.x * bt - 2 * at * at * C1.y;
        phi2 =
            at * at * C1.x * C1.x + at * at * C1.y * C1.y - at * at * R1 * R1;
        AAA = 4 * bt * bt - 4 * AA;
        BBB = 4 * bt * phi1 - 8 * AA * at * C1.x;
        CCC = phi1 * phi1 - 4 * AA * phi2;

        delta3 = BBB * BBB - 4 * AAA * CCC;
        delta3 = sqrt(delta3);

        ct1 = (-BBB + delta3) / (2 * AAA);
        ct2 = (-BBB - delta3) / (2 * AAA);

        PTTT[0].A = at;
        PTTT[0].B = bt;
        PTTT[0].C = ct1;
        PTTT[1].A = at;
        PTTT[1].B = bt;
        PTTT[1].C = ct2;
      }
    }
  } else // khac bk
  {
    if (kc2tam < hieu2bk) // long nhau ko co tt
      return;
    else if (kc2tam == hieu2bk) // tiep xuc trong 4 tiep tuyen trung nhau
    {
      if (ac == 0) // C1C2 song song truc hoanh
      {
        for (int i = 0; i < 4; i++) {
          PTTT[i].A = 1;
          PTTT[i].B = 0;
          PTTT[i].C = gd1.x;
        }
      } else if (bc == 0) // C1C2 song song truc tung
      {
        for (int i = 0; i < 4; i++) {
          PTTT[i].A = 0;
          PTTT[i].B = 1;
          PTTT[i].C = gd1.y;
        }
      } else if (ac != 0 && bc != 0) {
        for (int i = 0; i < 4; i++) {
          PTTT[i].A = bc;
          PTTT[i].B = -ac;
          PTTT[i].C = -bc * gd1.x + ac * gd1.y;
        }
      }
    } else if (kc2tam >
        hieu2bk) // 2 dtr cat nhau va roi nhau => co 2 duong tt ngoai
    {
      Point K; // giao diem cua 2 dg tt ngoai voi duong noi 2 tam dgtr
      double cR = R1 / R2, P1, Q1, M1, N1, L1, delta1, k1, k2;
      K.x = (cR * C2.x - C1.x) / (cR - 1);
      K.y = (cR * C2.y - C1.y) / (cR - 1);

      P1 = C1.x - K.x;
      Q1 = C1.y - K.y;

      M1 = P1 * P1 - R1 * R1;
      N1 = P1 * Q1;
      L1 = Q1 * Q1 - R1 * R1;

      delta1 = 4 * N1 * N1 - 4 * L1 * M1;

      if (delta1 < 0)
        return; // vo no
      else if (delta1 == 0) {
        k1 = k2 = (-2 * N1) / (2 * L1);
      } else {
        delta1 = sqrt(delta1);
        k1 = (-2 * N1 + delta1) / (2 * L1);
        k2 = (-2 * N1 - delta1) / (2 * L1);
      }

      PTTT[0].A = 1;
      PTTT[0].B = k1;
      PTTT[0].C = -K.x - PTTT[0].B * K.y;

      PTTT[1].A = 1;
      PTTT[1].B = k2;
      PTTT[1].C = -K.x - PTTT[1].B * K.y;
    }
  }

  /// VE TIEP TUYEN TRONG
  if (R1 == R2) // cung bk
  {
    if (kc2tam == tong2bk) // tiep xuc ngoai
    {
      if (ac == 0) {
        PTTT[2].A = PTTT[3].A = 1;
        PTTT[2].B = PTTT[3].B = 0;
        PTTT[2].C = PTTT[3].C = (C1.x + C2.x) / 2;
      } else if (bc == 0) {
        PTTT[2].A = PTTT[3].A = 0;
        PTTT[2].B = PTTT[3].B = 1;
        PTTT[2].C = PTTT[3].C = (C1.y + C2.y) / 2;
      } else if (ac != 0 && bc != 0) {
        PTTT[2].A = PTTT[3].A = bc;
        PTTT[2].B = PTTT[3].B = -ac;
        PTTT[2].C = PTTT[3].C = -bc * gd1.x + ac * gd1.y;
      }
    } else // cat tai 2 diem , roi nhau
    {
      if (kc2tam < tong2bk)
        return; // 2 duong tron cat nhau tai 2 diem, ko co tt trong
      else {
        Point J; // giao diem cua 2 dg tt trong voi duong noi 2 tam dgtr
        double cR1 = R1 / R2, P2, Q2, M2, N2, L2, delta2, k3, k4;
        J.x = (cR1 * C2.x + C1.x) / (cR1 + 1);
        J.y = (cR1 * C2.y + C1.y) / (cR1 + 1);

        P2 = C1.x - J.x;
        Q2 = C1.y - J.y;

        M2 = P2 * P2 - R1 * R1;
        N2 = P2 * Q2;
        L2 = Q2 * Q2 - R1 * R1;

        delta2 = 4 * N2 * N2 - 4 * L2 * M2;

        if (delta2 < 0)
          return; // vo no
        else if (delta2 == 0) {
          k3 = k4 = (-2 * N2) / (2 * L2);
        } else {
          delta2 = sqrt(delta2);
          k3 = (-2 * N2 + delta2) / (2 * L2);
          k4 = (-2 * N2 - delta2) / (2 * L2);
        }

        PTTT[2].A = 1;
        PTTT[2].B = k3;
        PTTT[2].C = -J.x - PTTT[2].B * J.y;

        PTTT[3].A = 1;
        PTTT[3].B = k4;
        PTTT[3].C = -J.x - PTTT[3].B * J.y;
      }
    }
  } else

  /// khac bk
  {
    if (kc2tam == tong2bk) // tiep xuc ngoai
    {
      if (ac == 0) {
        PTTT[2].A = PTTT[3].A = 1;
        PTTT[2].B = PTTT[3].B = 0;
        PTTT[2].C = PTTT[3].C = C1.x < C2.x ? (C1.x + R1) : (C2.x + R2);
      } else if (bc == 0) {
        PTTT[2].A = PTTT[3].A = 0;
        PTTT[2].B = PTTT[3].B = 1;
        PTTT[2].C = PTTT[3].C = C1.y < C2.y ? (C1.y + R1) : (C2.y + R2);
      } else if (ac != 0 && bc != 0) {
        PTTT[2].A = PTTT[3].A = bc;
        PTTT[2].B = PTTT[3].B = -ac;
        PTTT[2].C = PTTT[3].C = -bc * gd1.x + ac * gd1.y;
      }
    } else // cat tai 2 diem , roi nhau
    {
      if (kc2tam < tong2bk)
        return; // 2 duong tron cat nhau tai 2 diem, ko co tt trong
      else {
        Point J; // giao diem cua 2 dg tt trong voi duong noi 2 tam dgtr
        double cR1 = R1 / R2, P2, Q2, M2, N2, L2, delta2, k3, k4;
        J.x = (cR1 * C2.x + C1.x) / (cR1 + 1);
        J.y = (cR1 * C2.y + C1.y) / (cR1 + 1);

        P2 = C1.x - J.x;
        Q2 = C1.y - J.y;

        M2 = P2 * P2 - R1 * R1;
        N2 = P2 * Q2;
        L2 = Q2 * Q2 - R1 * R1;

        delta2 = 4 * N2 * N2 - 4 * L2 * M2;

        if (delta2 < 0)
          return; // vo no
        else if (delta2 == 0) {
          k3 = k4 = (-2 * N2) / (2 * L2);
        } else {
          delta2 = sqrt(delta2);
          k3 = (-2 * N2 + delta2) / (2 * L2);
          k4 = (-2 * N2 - delta2) / (2 * L2);
        }

        PTTT[2].A = 1;
        PTTT[2].B = k3;
        PTTT[2].C = -J.x - PTTT[2].B * J.y;

        PTTT[3].A = 1;
        PTTT[3].B = k4;
        PTTT[3].C = -J.x - PTTT[3].B * J.y;
      }
    }
  }
}

Map<String, dynamic> Giaodiemcua2duongtron(
    Point C1, Point C2, double R1, double R2) {
  double a, b, c, d, e, i1, i2, t1, t2, delta;
  var gd1 = Point();
  var gd2 = Point();
  int no;

  if (C1.x == C2.x && C1.y == C2.y && R1 == R2) {
    no = -1; // vo so no
    return {'no': no};
  }

  a = 2 * R1 * (C1.x - C2.x);
  b = 2 * R1 * (C1.y - C2.y);
  c = pow(R2, 2) - pow(C1.x - C2.x, 2) - pow(C1.y - C2.y, 2) - pow(R1, 2);

  delta = 4 * b * b + 4 * a * a - 4 * c * c;

  if (delta < 0) {
    no = 0; // ko co no
    return {'no': no};
  } else if (delta == 0) {
    t1 = t2 = b / (a + c);
    i1 = i2 = atan(t1) * 2;
    gd1.x = gd2.x = R1 * cos(i1) + C1.x;
    gd1.y = gd2.y = R1 * sin(i1) + C1.y;
    no = 1;

    return {'no': no, 'gd1': gd1};
  } else {
    d = -2 * b / (2 * (-a - c));
    e = sqrt(delta) / (2 * (-a - c));

    t1 = d + e;
    t2 = d - e;

    i1 = atan(t1) * 2;
    i2 = atan(t2) * 2;

    gd1.x = R1 * cos(i1) + C1.x;
    gd1.y = R1 * sin(i1) + C1.y;

    gd2.x = R1 * cos(i2) + C1.x;
    gd2.y = R1 * sin(i2) + C1.y;

    no = 2;

    var res = {'no': no, 'gd1': gd1, 'gd2': gd2};
    // print('Giaodiemcua2duongtron: $res');
    return res;
  }
}

double Khoangcachtu1diemden1doanthang(Point p, Point p0, Point p1) {
  double kc;
  double a = p1.y - p0.y, b = p0.x - p1.x;
  kc = (b * (p.y - p0.y) + a * (p.x - p0.x)) / sqrt(a * a + b * b);
  return kc;
}
*/

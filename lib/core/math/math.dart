import 'dart:math' as math;

export 'affineTransform.dart';
export 'geometric.dart';
export 'objectives.dart';

double sqrt(double a) => math.sqrt(a);
double pow(double a, double b) => math.pow(a, b);
double abs(double a) => a < 0 ? -a : a;
int round(double a) => a.round();
double atan(double a) => math.atan(a);
double cos(double a) => math.cos(a);
double sin(double a) => math.sin(a);
const pi = math.pi;

const kDegToRad = math.pi / 180.0;
double toRad(double degree) => degree * kDegToRad;

double factorial(double x) {
  if (x < 3) {
    if (x < 2)
      return 1;
    else
      return 2;
  } else {
    double res = 1;
    for (double i = 2; i <= x; i++) {
      res *= i;
    }
    return res;
  }
}

const int sign_mask = 0x80000000;
const double b = 0.596227;
double atan2(double y, double x) {
  // Extract the sign bits
  int uxS = sign_mask & x.round();
  int uyS = sign_mask & y.round();

  // Determine the quadrant offset
  double q = ((~uxS & uyS) >> 29 | uxS >> 30).toDouble();

  // Calculate the arctangent in the first quadrant
  double bxyA = abs(b * x * y);
  double num = bxyA + y * y;
  double atan1Q = num / (x * x + bxyA + num);

// Translate it to the proper quadrant
  int uatan2Q = (uxS ^ uyS) | atan1Q.round();
  return q + uatan2Q.toDouble();
}

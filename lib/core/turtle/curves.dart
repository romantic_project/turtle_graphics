import 'dart:ui';

import 'package:flutter_turtle_graphics/core/math/complex.dart';
import 'package:flutter_turtle_graphics/core/math/math.dart';
import 'package:flutter_turtle_graphics/core/turtle/path.dart';
import 'package:flutter_turtle_graphics/core/utils/graphics_extension.dart';

extension TurtlePathConverter on TurtlePath {
  Path get toPath {
    centering();
    var path = Path();
    int length = ps.length;
    if (length > 1) {
      for (var i = 0; i < length; ++i) {
        var o = ps[i];
        if (routes.contains(o)) {
          path.moveTo(o.x, o.y);
        } else {
          path.lineTo(o.x, o.y);
          if (i > 0) {
            var po = ps[i - 1];
            if (!routes.contains(o)) {
              path.moveTo(po.x, po.y);
            }
          }
        }
      }
    }
    return path;
  }
}

extension TurtleCurve on TurtlePath {
  TurtlePath genCurveC(double a, int N) {
    double aV2_2 = a * (sqrt(2) / 2);
    if (N > 1) {
      genCurveC(aV2_2, N - 1);
      turnRight(90);
      genCurveC(aV2_2, N - 1);
      turnRight(-90);
    } else {
      lineForward(a);
    }
    return this;
  }

  TurtlePath genCurveDragon(double a, int N, int dir) {
    double aV2_2 = a * (sqrt(2) / 2);
    if (N > 1) {
      turnRight((-45 * dir).toDouble());
      genCurveDragon(aV2_2, N - 1, 1);
      turnRight((90 * dir).toDouble());
      genCurveDragon(aV2_2, N - 1, -1);
      turnRight((-45 * dir).toDouble());
    } else {
      lineForward(a);
    }
    return this;
  }

  TurtlePath genKoch(double a, int N, double alpha) {
    assert(alpha <= 60);
    if (N > 1) {
      a /= 3;
      genKoch(a, N - 1, alpha);
      turnRight(-alpha);
      genKoch(a, N - 1, alpha);
      turnRight(alpha * 2);
      genKoch(a, N - 1, alpha);
      turnRight(-alpha);
      genKoch(a, N - 1, alpha);
    } else {
      lineForward(a);
    }
    return this;
  }

  /// bat chuoc Gasket
  TurtlePath genCurveSierpinskiTriangle(
      Point p, double a, double theta, int N) {
    if (N > 1) {
      a /= 2;
      double alpha = theta * kDegToRad;
      genCurveSierpinskiTriangle(p, a, theta, N - 1);
      var p2 = Point(
          p.x + (a * cos(pi / 3 + alpha)), p.y - (a * sin(pi / 3 + alpha)));
      genCurveSierpinskiTriangle(p2, a, theta, N - 1);
      var p3 = Point(p.x + (a * cos(alpha)), p.y - (a * sin(alpha)));
      genCurveSierpinskiTriangle(p3, a, theta, N - 1);
    } else {
      moveTo(p);
      setCD(theta);
      lineForward(a);
      turnRight(-120);
      lineForward(a);
      turnRight(-120);
      lineForward(a);
      turnRight(-120);
    }
    return this;
  }

  TurtlePath genSpiral(double alpha, double deltaAlpha, double distance,
      double deltaDistance, int count) {
    var da = alpha;
    var dd = distance;
    moveTo(Point(1, 1));
    for (var i = 0; i < count; ++i) {
      turnRight(da);
      lineForward(dd);
      da += deltaAlpha;
      dd += deltaDistance;
    }
    return this;
  }
}

/*TurtlePath genSpiralPolygon(TurtlePath path, double alpha, double deltaAlpha,
    double distance, double deltaDistance, int count) {
  final originPoints = List.from(path.ps.map((e) => Point(e.x, e.y)));
  var points = <Point>[];

  var da = alpha;
  var dd = distance;
  for (var i = 0; i < count; ++i) {
    path.turnRight(da);
    path.lineForward(dd);

    var polygon = Polygon(List.from(originPoints.map((e) => Point(e.x, e.y))));

    var m = AffineTransform.identity();
    m.forward(dd, path.cd);
    m.rotateOrigin(path.cd);
    m.applyPolygon(polygon);

    points.addAll(polygon.vertices);

    da += deltaAlpha;
    dd += deltaDistance;
  }

  return TurtlePath(points);
}*/

void _genPentaCircle(List<Circle> g, Point center, double R, int N) {
  if (N > 1) {
    var affine = AffineTransform.identity();
    double rn =
        R * sin(36 * kDegToRad) * cos(72 * kDegToRad) / sin(72 * kDegToRad) +
            R -
            R * cos(36 * kDegToRad);
    double dx = rn * cos(36 * kDegToRad) / sin(36 * kDegToRad);
    affine.translate(dx, -rn);
    var c = Point(center.x, center.y);
    affine.apply(c);
    _genPentaCircle(g, c, rn, N - 1);

    affine = AffineTransform.identity();
    affine.rotate(72, c);
    c = Point(center.x, center.y);
    affine.apply(c);
    _genPentaCircle(g, c, rn, N - 1);

    affine = AffineTransform.identity();
    affine.rotate(72, c);
    c = Point(center.x, center.y);
    affine.apply(c);
    _genPentaCircle(g, c, rn, N - 1);

    affine = AffineTransform.identity();
    affine.rotate(72, c);
    c = Point(center.x, center.y);
    affine.apply(c);
    _genPentaCircle(g, c, rn, N - 1);

    affine = AffineTransform.identity();
    affine.rotate(72, c);
    c = Point(center.x, center.y);
    affine.apply(c);
    _genPentaCircle(g, c, rn, N - 1);
  } else {
    g.add(Circle(center, R));
  }
}

Path genPentaCircle(Point center, double R, int N) {
  var path = Path();
  var circles = <Circle>[];
  _genPentaCircle(circles, center, R, N);
  for (var o in circles) {
    path.addOval(Rect.fromCircle(center: o.center.toOffset, radius: o.R));
  }
  return path;
}

Path newton() {
  var path = Path();
  int maxCount = 255;
  // int multCol = 15;
  double tol = .0001;
  // std::complex<double> r1(1,               0);
  // std::complex<double> r2(-0.5,  sin(2*pi/3));
  // std::complex<double> r3(-0.5, -sin(2*pi/3));
  var r1 = Complex(1, 0);
  var r2 = Complex(-0.5, sin(2 * pi / 3));
  var r3 = Complex(-0.5, -sin(2 * pi / 3));

  // ramCanvas4c8b theRamCanvas = ramCanvas4c8b(4096, 4096, -2.0, 2, -2, 2);
  int maxX = 4096;
  int maxY = 4096;

  for (int y = 0; y < maxY; y++) {
    for (int x = 0; x < maxX; x++) {
      var z = Complex(x.toDouble(), y.toDouble());
      // std::complex<double> z(theRamCanvas.int2realX(x), theRamCanvas.int2realY(y));
      int count = 0;
      while ((count < maxCount) &&
          ((z.subtract(r1)).abs() >= tol) &&
          ((z.subtract(r2)).abs() >= tol) &&
          ((z.subtract(r3)).abs() >= tol)) {
        if ((z).abs() > 0) {
          var a = z.pow(2);
          var b = z.pow(3);
          z = z.subtract(b.subtract(Complex(1, 0))).divide(a.multiply(3.0));
        }
        count++;
      }

      if ((z.subtract(r1)).abs() < tol)
        path.addOval(Rect.fromCircle(
            center: Offset(x.toDouble(), y.toDouble()), radius: 2));
      // theRamCanvas.drawPoint(x, y, color4c8b(255-count*MultCol, 0, 0));
      if ((z.subtract(r2)).abs() <= tol)
        path.addOval(Rect.fromCircle(
            center: Offset(x.toDouble(), y.toDouble()), radius: 2));
      // theRamCanvas.drawPoint(x, y, color4c8b(0, 255-count*MultCol, 0));
      if ((z.subtract(r3)).abs() <= tol)
        path.addOval(Rect.fromCircle(
            center: Offset(x.toDouble(), y.toDouble()), radius: 2));
      // theRamCanvas.drawPoint(x, y, color4c8b(0, 0, 255-count*MultCol));
    }
  }

  return path;
}

import 'package:flutter/material.dart';
import 'package:flutter_turtle_graphics/core/turtle/path.dart';
import 'package:flutter_turtle_graphics/core/utils/graphics_extension.dart';
class TurtlePainter extends CustomPainter {
  final TurtlePath turtlePath;
  final Path path;
  final bool debugging;

  TurtlePainter({
    this.debugging = true,
    this.path,
    this.turtlePath,
  });

  @override
  void paint(Canvas canvas, Size size) {
    canvas.save();
    var center = size.center(Offset.zero);
    canvas.translate(center.dx - (turtlePath.maxX - turtlePath.minX) / 2,
        center.dy - (turtlePath.maxY - turtlePath.minY) / 2);
    if (path != null) {
      canvas.drawPath(
          path,
          Paint()
            ..color = randomColor()
            ..strokeWidth = 2
            ..style = PaintingStyle.stroke);
    }
    canvas.restore();
    if (debugging) {
      canvas.drawCircle(
          center,
          4,
          Paint()
            ..color = Colors.red
            ..style = PaintingStyle.fill);
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

import 'dart:math';

import 'package:flutter_turtle_graphics/core/math/math.dart';

class TurtlePath {
  var ps = <Point>[];
  var cp = Point.zero;
  var cd = 0.0;
  var minX = 0.0, minY = 0.0, maxX = 0.0, maxY = 0.0;
  var routes = <Point>[];

  TurtlePath();

  void setPixel(int x, int y) {
    ps.add(Point(x.toDouble(), y.toDouble()));
  }

  void set8Pixel(int x, int y, int xc, int yc) {
    setPixel(x + xc, y + yc);
    setPixel(-x + xc, y + yc);
    setPixel(x + xc, -y + yc);
    setPixel(-x + xc, -y + yc);

    setPixel(y + xc, x + yc);
    setPixel(y + xc, -x + yc);
    setPixel(-y + xc, x + yc);
    setPixel(-y + xc, -x + yc);
  }

  void set4Pixel(int x, int y, int xc, int yc) {
    setPixel(x + xc, y + yc);
    setPixel(x + xc, -y + yc);
    setPixel(-x + xc, y + yc);
    setPixel(-x + xc, -y + yc);
  }

  void moveTo(Point p) {
    cp = Point(p.x, p.y);
    routes.add(Point(cp.x, cp.y));
  }

  void line(int x0, int y0, int x1, int y1) {
    int dx, dy, P, dP1, dP2;
    int incx, incy, x, y;

    if (x0 < x1)
      incx = 1;
    else
      incx = -1;
    if (y0 < y1)
      incy = 1;
    else
      incy = -1;

    if (x0 == x1) {
      x = x0;
      for (y = y0; y != y1; y += incy) {
        setPixel(x, y);
      }
      return;
    }

    if (y0 == y1) {
      y = y0;
      for (x = x0; x != x1; x += incx) {
        setPixel(x, y);
      }
      return;
    }

    dx = abs((x1 - x0).toDouble()).round();
    dy = abs((y1 - y0).toDouble()).round();

    if (dy <= dx) {
      P = -2 * (dy) + (dx);
      dP1 = -2 * (dy);
      dP2 = 2 * (dx) - 2 * (dy);

      y = y0;
      for (x = x0; x != x1; x += incx) {
        setPixel(x, y);
        if (P >= 0)
          P = P + dP1;
        else {
          P = P + dP2;
          y = y + incy;
        }
      }
    } else {
      P = -2 * (dx) + (dy);
      dP1 = -2 * (dx);
      dP2 = 2 * (dy) - 2 * (dx);

      x = x0;
      for (y = y0; y != y1; y += incy) {
        setPixel(x, y);
        if (P >= 0)
          P = P + dP1;
        else {
          P = P + dP2;
          x = x + incx;
        }
      }
    }
  }

  void lineTo(Point p) {
    line(cp.x.round(), cp.y.round(), p.x.round(), p.y.round());
    moveTo(p);
  }

  void circle(int xc, int yc, int R) {
    if (R < 0) R = -R;
    int P = 3 - 2 * R;
    int x, y = R;
    /////  Dung gia so bac 1 //////
    /*
	for(x=0;x<=y;x++)
	{
		Set8Pixel(x,y,xc,yc);
		if(P<=0)
			P=P+4*x+6;
		else
		{
			P=P+4*(x-y)+10;
			y--;
		}
	}
	*/
    ///////////////////////////////
    ///// Dung gia so bac 2 ///////
    int dP1 = 6, dP2 = 10 - 4 * R;
    for (x = 0; x <= y; x++) {
      set8Pixel(x, y, xc, yc);
      if (P <= 0)
        P = P + dP1;
      else {
        P = P + dP2;
        y--;
        dP2 = dP2 + 4;
      }
      dP1 = dP1 + 4;
      dP2 = dP2 + 4;
    }
    ///////////////////////////////
  }

  void ellipse(int xc, int yc, int a, int b) {
    ///  ty le giua a va b = 60/33 ////
    int x, y;
    //////   VE PHAN SOAI  ////////////////////////////////
    int ps = a * a + 2 * b * b - 2 * a * a * b;
    //////  Dung gia so bac 1  ///////
    /*
	for(x=0,y=b; b*b*x <= a*a*y ; x++)
	{
		set4Pixel(x,y,xc,yc);
		if(Ps<=0)
			Ps= Ps + 4*b*b*x + 6*b*b;
		else
		{
			Ps= Ps + 4*(b*b*x - a*a*y) + 4*a*a + 6*b*b;
			y--;
		}
	}
	*/
    //////////////////////////////////
    //////  Dung gia so bac 2  ///////

    int dPs1 = 6 * b * b, dPs2 = 6 * b * b + 4 * a * a - 4 * a * a * b;
    x = 0;
    y = b;
    for (; b * b * x <= a * a * y; x++) {
      set4Pixel(x, y, xc, yc);
      if (ps <= 0)
        ps = ps + dPs1;
      else {
        ps = ps + dPs2;
        dPs2 = dPs2 + 4 * a * a;
        y--;
      }
      dPs1 = dPs1 + 4 * b * b;
      dPs2 = dPs2 + 4 * b * b;
    }

    ///////////////////////////////////////////////////////

    //////   VE PHAN DOC   ////////////////////////////////
    int pd = a * a + 2 * b * b - 2 * a * a * b;
    //////  Dung gia so bac 1  ///////
    /*
	for(y=0,x=a;b*b*x > a*a*y ; y++)
	{
		set4Pixel(x,y,xc,yc);
		if(Pd<=0)
			Pd= Pd + 4*a*a*y + 6*a*a;
		else
		{
			Pd= Pd + 4*(a*a*y - b*b*x) + 4*b*b + 6*a*a;
			x--;
		}
	}
	*/
    //////////////////////////////////
    //////  Dung gia so bac 2  ///////

    int dPd1 = 6 * a * a, dPd2 = 6 * a * a + 4 * b * b - 4 * b * b * a;
    y = 0;
    x = a;
    for (; b * b * x > a * a * y; y++) {
      set4Pixel(x, y, xc, yc);
      if (pd <= 0)
        pd = pd + dPd1;
      else {
        pd = pd + dPd2;
        dPd2 = dPd2 + 4 * b * b;
        x--;
      }
      dPd1 = dPd1 + 4 * a * a;
      dPd2 = dPd2 + 4 * a * a;
    }

    //////////////////////////////////
    ///////////////////////////////////////////////////////
  }

  void lineRel(double dx, double dy) {
    lineTo(Point(cp.x + dx, cp.y + dy));
  }

  void arc(double xc, double yc, double R, double start, double open,
      bool chieu, double N) {
    // start la goc tao voi Ox theo chieu duong la kim dong ho
    // open la goc mo tu vi tri xuat phat
    // end la goc tao voi Ox theo chieu duong la kim dong ho

    start *= kDegToRad;
    open *= kDegToRad;
    double end;
    double inc = open / N;

    moveTo(Point(xc + R * cos(start), yc + R * sin(start)));

    if (chieu) {
      end = start + open;
      for (; start < end; start += inc) {
        lineTo(Point(xc + R * cos(start), yc + R * sin(start)));
      }
      lineTo(Point(xc + R * cos(end), yc + R * sin(end)));
    } else {
      end = start - open;
      for (; start > end; start -= inc) {
        lineTo(Point(xc + R * cos(start), yc + R * sin(start)));
      }
      lineTo(Point(xc + R * cos(end), yc + R * sin(end)));
    }
  }

  void setCD(double alpha) {
    cd = alpha * kDegToRad;
  }

  Point lineForward(double distance) {
    double dx = distance * cos(cd), dy = distance * sin(cd);
    Point next = Point(cp.x + dx, cp.y + dy);
    lineTo(next);
    return next;
  }

  void turnRight(double alpha) {
    cd += alpha * kDegToRad;
  }

  void _findMinMax() {
    minX = 0;
    minY = 0;
    maxX = 0;
    maxY = 0;
    for (var o in ps) {
      minX = min(o.x, minX);
      minY = min(o.y, minY);
      maxX = max(o.x, maxX);
      maxY = max(o.y, maxY);
    }
  }

  Point get center {
    _findMinMax();
    var center = Point();
    center.x = (maxX - minX) * 0.5;
    center.y = (maxY - minY) * 0.5;
    return center;
  }

  double get width {
    _findMinMax();
    var w = abs(minX) + maxX;
    return w;
  }

  double get height {
    _findMinMax();
    var h = abs(minY) + maxY;
    return h;
  }

  void centering() {
    _findMinMax();
    var t = AffineTransform.identity();
    t.translate(abs(minX), abs(minY));
    t.applyPoints(ps);
    t.applyPoints(routes);
  }
}

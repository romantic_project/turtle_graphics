import 'dart:math' as math;
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_turtle_graphics/core/math/math.dart';

Color randomColor([bool withAlpha = true]) {
  var ran = math.Random.secure();
  var a = withAlpha ? ran.nextInt(255) : 255;
  var g = ran.nextInt(255);
  var b = ran.nextInt(255);
  var r = ran.nextInt(255);
  return Color.fromARGB(a, r, g, b);
}

extension OffsetConverter on Offset {
  Point get toPoint => Point(dx, dy);
}

extension PointConverter on Point {
  Offset get toOffset => Offset(x, y);
}
